Authenticator
=============

This library is extension for [Nette framework](www.nette.org).

Library helping you with usecases

- register user
- send valid email
- login user
- prepare multi authenticator
- forgot password, send token and verify
- synchronize identity cross browser

Best way hou to install is via composer:

```sh
composer require "h4kuna/authenticator"
```

How start?
---------
Define extension in your neon.
```sh
extensions:
	cms: h4kuna\Cms\DI\AuthenticatorExtension

# all options
cms:
	identity:
		removeColumns: ['password', 'block'] # optional
	loginForm:
		nicknameIsEmail: TRUE # optional
```

Services
--------
- authenticator
- texts (ala translator, bud very simple) you can use other
- formFactory
- cacheFactory
- AuthenticatorFacadeInterface this you must implemnt
- loginForm
- forgotPasswordForm
- synchronizeIdentity
- identityFactory

What this library does not do
-----------------------------
- define database layout
- define cache storage
- layout of forms
- define presenters
