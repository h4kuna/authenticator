Authenticator
=============

Knihovna je napsaná pro [Nette framework](www.nette.org) a PHP 5.4+.

K instalaci použijte composer:
```sh
composer require h4kuna/authenticator
```

S čím knihovna pomůže
------------------
Postará se kompletně o uživatele od jeho registace, přihlášení, zapomenuté heslo, obnovu hesla a synchronizace identity, kde doporučuji implementovat redis storage a použít.

Co knihovna neřeší
-----------------
Je napsaná obecně, aby se dala lehce upravovat a nikoho netlačila do věcí co nechce. Neřeší následující:

- vykreslování formulářů
- storage pro ukládání (doporučuji redis)
- presentery
- posílání emailů

# Jak integrovat do sandboxu

Stažení Nette sandboxu
```sh
composer create-project nette/sandbox demo-app
```
Databázi pro příklad jsem použil MySql, jen protože je nejpoužívanější. :)

Nezapomeňte si vyplnit údaje pro spojení s databází v *app/config/config.local.neon*.
```sh
database:
	dsn: 'mysql:host=127.0.0.1;dbname=test'
	user:
	password:
```

Tabulku uživatelů v databázi budeme mít:
```sql
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255),
  `block` date NULL,
  `login_count` int NOT NULL
) ENGINE='InnoDB';

ALTER TABLE `users`
ADD UNIQUE `name` (`name`),
ADD UNIQUE `email` (`email`);

CREATE TABLE `user_password` (
  `user_id` bigint(20) unsigned NOT NULL,
  `hash` varchar(32) NOT NULL,
  `action` enum('registration','restore') NOT NULL DEFAULT 'restore',
  `insert_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';
```
Zaregistrujeme rošíření v neonu.
```sh
extensions:
	authlib:
		h4kuna\Cms\DI\AuthenticatorExtension

authlib:
	identity: # volitelné
		removeColumns: ['password', 'block'] # jaké hodnoty chcete odebrat před tím než se uloží do idntity, výchozí jsou [id, password]
		passwordLength: 5 # délka hesla v registraci a obnovení
```

Implementujeme si [AuthenticatorFacadeInterface](../src/Core/Security/AuthenticatorFacadeInterface.php). Můžeme se nechat inspirovat [UserModel](tests/libs/UserModel.php) třídou pro testování. Umístíme například do *app/Model*

```php
<?php

namespace App\Model;

use Nette\Database,
	h4kuna\Cms\Core\Security,
	Nette\Security as NSecurity,
   Nette\Utils;

class AuthenticatorFacade implements \h4kuna\Cms\Core\Security\AuthenticatorFacadeInterface
{

	/** @var Database\Context */
	private $context;

	public function __construct(Database\Context $context)
	{
		$this->context = $context;
	}

	public function fetchUserById($id)
	{
		return $this->table()->where('id', $id)->fetch();
	}

	public function fetchUserByUsername($username)
	{
		return $this->table()->where('email = ? OR name = ?', $username, $username)->fetch();
	}

	public function createAuthenticatorStructure($data)
	{
		if ($data) {
			$rawData = $data->toArray();
		} else {
			$rawData = [
				'id' => NULL,
				'block' => NULL,
			];
		}
		$struct = new Security\AuthenticatorStructure($rawData, $rawData['block'], $rawData['id']);
		$struct->setPassword($data['password']);
		return $struct;
	}

	public function loginSuccess(NSecurity\User $user, $method)
	{
		if ($method === 'reload') {
			return;
		}
		$this->table()->where('id', $user->getId())->update(['login_count' => new Database\SqlLiteral('login_count + 1')]);
	}

	public function restorePassword($hash, $userData, $action = 'restore')
	{
		$this->context->table('user_password')->where('user_id', $userData->id)->where('action', 'restore')->delete();

		$this->context->table('user_password')->insert([
			'user_id' => $userData->id,
			'hash' => $hash,
			'action' => $action
		]);

		return $userData->toArray();
	}

	public function register($hash, $values)
	{
		$userId = $this->table()->insert([
			'name' => $values->username ? : NULL,
			'password' => $values->password,
			'email' => $values->email
		]);

		$userData = $this->fetchUserById($userId);
		return $this->restorePassword($hash, $userData, 'registration');
	}

	public function isEmailUnique($email)
	{
		return !$this->table()->where('email', $email)->fetch();
	}

	public function isUsernameUnique($username)
	{
		return !$this->table()->where('name', $username)->fetch();
	}

	private function table($table = 'users')
	{
		return $this->context->table($table);
	}

	public function updatePassword($hash, $password)
	{
		$data = $this->confirmHash($hash);
		if (!$data) {
			return FALSE;
		}
		$this->table()->where('id', $data->user_id)->update([
			'password' => $password
		]);
		return TRUE;
	}

	/** @rerturn ActiveRow|NULL */
	private function confirmHash($hash)
	{
		$data = $this->table('user_password')->where('hash', $hash)->fetch();
		if (!$data) {
			return NULL;
		} elseif (Utils\DateTime::from($data->insert_dt)->modify('+1 day') < new \DateTime('now')) {
			$this->table('user_password')->where('hash', $hash)->delete();
			return NULL;
		}

		if ($data->action === 'restore') {
			$this->table('user_password')->where('user_id', $data->user_id)->delete();
		} else {
			$this->table('user_password')->where('hash', $hash)->delete();
		}
		return $data;
	}

}
```

Zaregistrujeme si službu do *app/config/config.neon*:
```sh
services:
    - App\Model\AuthenticatorFacade
```

Z *app/Model/UserManager.php* smažme metodu **authenticate** a to že **implementuje IAuthenticator**.

## Registrace

Vytvoříme všechno v rámci jednoho presenteru *app/presenters/RegistrationPresenter.php*.
```php
<?php

namespace App\Presenters;

class RegistrationPresenter extends BasePresenter
{

	/** @var \h4kuna\Cms\Forms\RegistrationForm @inject */
	public $registrationForm;

	/** @var \h4kuna\Cms\Forms\LoginForm @inject */
	public $loginForm;

	/** @var \h4kuna\Cms\Forms\ForgotPasswordForm @inject */
	public $forgotPasswordForm;

	/** @var \h4kuna\Cms\Forms\RestorePasswordForm @inject */
	public $restorePasswordForm;

	public function actionLogout()
	{
		$this->user->logout();
		$this->flashMessage('Jste odhlášen.');
		$this->redirect('Homepage:');
	}

	public function renderRestorePassword($hash)
	{
		$template = $this->getTemplate();
		$template->hash = $hash;
	}

	protected function createComponentRestorePassword()
	{
		$this->restorePasswordForm->onSuccess[] = function () {
			$this->flashMessage('Heslo bylo uloženo. Přihlašte se.');
			$this->redirect('login');
		};
		$this->restorePasswordForm->onFail[] = function() {
			$this->flashMessage('Token na obnovu hesla vypršel, nechte si poslat nový.');
			$this->redirect('forgotPassword');
		};
		return $this->restorePasswordForm->create($this->getParameter('hash'));
	}

	protected function createComponentForgotPassword()
	{
		$this->forgotPasswordForm->onSuccess[] = function($hash, $data) {
			// send email with hash
			$this->flashMessage('Přesměrujeme Vás na stránku s obnovením hesla, normálně by vám byl poslán email s tokenem.');
			$this->redirect('restorePassword', ['hash' => $hash]);
		};
		$this->forgotPasswordForm->onUsernameNotFound[] = function() {
			$this->flashMessage('Takové uživatelské jméno u nás neexistuje.');
		};
		return $this->forgotPasswordForm->create();
	}

	protected function createComponentRegistration()
	{
		$this->registrationForm->onSuccess[] = function ($hash, $values) {
			// $mail->sendConfirmMessage($hash, $values);
			$this->flashMessage('Registrace byla úspěšná, přihlašte se.');
			$this->redirect('login');
		};
		return $this->registrationForm->create();
	}

	protected function createComponentLogin()
	{
		$this->loginForm->onSuccess[] = function (\Nette\Security\User $user, $remember) {
			$user->setExpiration($remember ? '+10 minute' : '+5 hours', !$remember);
			$this->flashMessage('Jste přihlášen.');
			$this->redirect('Homepage:');
		};
		$this->loginForm->onIdentityIsBlocked[] = function ($values) {
			$this->flashMessage('Váš účet byl zablokován.');
		};
		$this->loginForm->onIdentityNotFound[] = function ($values) {
			$this->flashMessage('Takové přihlašovací jméno u nás neexistuje.');
		};
		$this->loginForm->onInvalidPassword[] = function ($values) {
			$this->flashMessage('Heslo je špatně.');
		};
		$this->loginForm->onCatchException[] = function($values, $exception) {
			// check type exception and send flash message
		};

		return $this->loginForm->create();
	}

}
```

Zbývají nám už jen šablony.

Do *app/presenters/templates/@layout.latte* si přidáme odkazy.
```html
<div style="background-color: white;">
<a n:href="Homepage:">Home</a> |
{if $user->isLoggedIn()}
	<a n:href="Registration:logout">Odhlásit</a>
{else}
	<a n:href="Registration:">Registrace</a> |
	<a n:href="Registration:login">Přihlášení</a> |
	<a n:href="Registration:forgotPassword">Zapomenuté heslo</a>
{/if}
</div>
```
Ostatní jsou umístěny v *app/presenters/templates/Registration*. Pouze v nich vyvoláme komponenty, případně si je můžete manuálně vykreslit.
#### default.latte
```html
{block content}
{control registration}
```

#### forgotPassword.latte
```html
{block content}
{control forgotPassword}
```

#### login.latte
```html
{block content}
{control login}
```

#### restorePassword.latte
```html
{block content}
<p>Token: {$hash}</p>
{control restorePassword}
```

Nyní pusťte aplikaci na homepage budou odkazy, tak se zkuste zaregistrovat, přihlásit, odhlásit a obnovit heslo.

## Synchronizace identity
Ukážeme si jak funguje synchronizece identity napříč všemi zařízeními. Vyrobíme si editační form pro *Nick*.

Presenter
```php
<?php

namespace App\Presenters;

class ProfilePresenter extends BasePresenter
{

	/** @var \h4kuna\Cms\Core\Form\FormFactory @inject */
	public $formFactory;

	/** @var \h4kuna\Cms\Core\Security\SynchronizeIdentity @inject */
	public $synchronizeIdentity;

	/** @var \App\Model\UserManager @inject */
	public $userManager;

	protected function startup()
	{
		parent::startup();
		if(!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:');
		}
	}

	protected function createComponentProfile()
	{
		$form = $this->formFactory->create(TRUE);
		$form->addText('username', 'Nick')
			->setDefaultValue($this->getUser()->getIdentity()->name);

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'successForm'];
		return $form;
	}

	public function successForm($form, $values)
	{
		$this->userManager->saveNick($this->getUser()->getId(), $values->username);
		$this->synchronizeIdentity->reloadLoggedUser();
		$this->flashMessage('Uloženo, koukněte do identity.');
		$this->redirect('this');
	}

}
```
Do @layout.latte si přidáme odkaz na nový presenter, umístíme ho před odkaz Odhlásit.
```html
<a n:href="Profile:">Profil</a> |
```

Šablona *app/presenters/templates/Profile/default.latte*
```html
{block content}
{control profile}
```

Do UserManage.php vytvoříme metodu kterou voláme v ProfilePresenteru.
```php
public function saveNick($userId, $username)
{
	$this->database->table(self::TABLE_NAME)->where('id', $userId)->update(['name' => $username]);
}
```

Pokud se přihlásíte více zařízeními a v jednom upravíme *Nick* přes formulář v ProfilePresenter, tak po obnovení stránek v ostatních zařízeních bude načtená nová hodnota.



# Stručnější popis služeb

Co je potřeba implementovat
--------------------------
[AuthenticatorFacadeInterface](../src/Core/Security/AuthenticatorFacadeInterface.php) je rozhraní které si napište podle svých potřeb. Koukněte a dodržte datové typy v anotacích. Službu si zaregistrujte ve svém neonu.

Metody:

- fetchUserById - vyhledejte uživatele podle jeho id a vraťte libovolnou strukturu ze které pak vyrobíte [AuthenticatorStructure](../src/Core/Security/AuthenticatorStructure.php)
- fetchUserByUsername - vyhledejte uživatel podle unikátního přihlašovacího jména, může být i email
- createAuthenticatorStructure - příjde vám vaše struktura a vytvořte objekt [AuthenticatorStructure](../src/Core/Security/AuthenticatorStructure.php).
- loginSuccess - zavolá se po úspěšném přihlášení, zvedněte si například login_counter nebo jiné statistiky, může být i prázdná.
- restorePassword - přijímá **hash** je to identifikátor požadavku, stejný dostanete i v callbacku, je dobré si ho uložit a podle něj pak hleda pro ověření emailu nebo obnovu hesla
- register - zase přijímá **hash** a hodnoty z formuláře, hash zase dostanete v callbacku **onSuccess**
- isEmailUnique a isUsernameUnique - se používaj pro registraci, validační metoda, aby ověřila duplicitu.


Jaké služby jsou k dispozici
---------------------------
- [cacheFactory](#cacheFactory)
- [textsFactory](#textsFactory)
- [texts](#texts)
- [authenticator](#authenticator)
- [synchronizeIdentity](#synchronizeIdentity)
- [identityFactory](#identityFactory)
- [formFactory](#formFactory)
- [loginForm](#loginForm)
- [forgotPasswordForm](#forgotPasswordForm)
- [registrationForm](#registrationForm)

Většina služeb má možnost navěsit události, nebo je podědit nebo je úplně nahradit, tím jak jsou pojmenovány.


```php
class MyCacheFactory extends \h4kuna\Cms\Core\Caching\CacheFactory
{
// content
}
```

```sh
services:
    authlib.cacheFactory:
        class: MyCacheFactory
```

### <a name="cacheFactory">cacheFactory</a>
Jednoduchá služba na vytváření cache. V tomto případě mužete podstrčit jiné uložiště [IStorage](https://api.nette.org/Nette.Caching.IStorage.html). Injektne se výchozí, které používáte, pokud budete chtít jiné.

```sh
myStorage: FooStorage # implementuje IStorage

authlib.cacheFactory:
    class: h4kuna\Cms\Core\Caching\CacheFactory(@myStorage)
```

### <a name="textsFactory">textsFactory</a>
Je třída starající se o texty do formulářů. Výchozí texty jsou v souboru [texts.neon](../src/texts.neon).
Pro přehlednost je všchno zapsané malými písmeny a mezeru určuje tečka nebo podsekce.

Následující zápisy jsou ekvivalentní. Záleží na Vás co se Vám bude lépe číst.
```sh
form:
    password:
        label: Heslo
        rule: 'Vyplňte heslo.'
```
```sh
form.password.label: Heslo
form.password.rule: 'Vyplňte heslo.'
```

Pokud budete chtít přepsat nějaké texty, udělejte si vlastní neon soubor a přidejte ho v konfiguračním neonu.
```sh
services:
	authlib.textsFactory:
		setup:
			- addFile(%tempDir%/../config/my-texts.neon)
```

### <a name="texts">texts</a>
Je třída starající se o texty v rámci knihovny. Definuje ji interface, takže můžete napojit svůj překladač, jen si uděláte mezi službu.

### <a name="authenticator">authenticator</a>
Tato třída se stará o přihlášení uživatele. Podporuje přihlášení přes id uživatel (v testech nebo pro reload identity), dále přihlášení přes heslo. Další metody přihlášení přidáte poděděním a rozšířením.

### <a name="synchronizeIdentity">synchronizeIdentity</a>
Bude-li uživatel přihlášen na více zařízeních v jeden moment a na jednom zařízení se mu změní identita, tak v ostatních zařízeních se také změní. Nebo rozhodnote-li se blokovat uživatele tímto ho ihned odhlásíte. Nasadí se nějaká novinka a všem budete potřebovat obnovit identitu, není problem.

Ovlivňuje jednotlivce, právě přihlášeného nebo všechny najednou.

### <a name="identityFactory">identityFactory</a>
Tvůrce identity a přidává tokeny pro [synchronizeIdentity](#synchronizeIdentity).

### <a name="identityFactory">formFactory</a>
Tvůrce instancí formulářů.

### <a name="loginForm">loginForm</a>
Třida na přihlašovací formulář, která má několik callbacků které si vyplňte, do každého můžete dát flash message a redirect podle potřeby. Případně pokud budete potřebovat reagovat na vlastní vyjímky.

### <a name="forgotPasswordForm">forgotPasswordForm</a>
Třída na formulář pro zapomenuté heslo. Zase vyplňte callbacky, třeba když budete chtí poslat email.

### <a name="registrationForm">registrationForm</a>
Třída na tvorbu registračního formuláře, avšak odhaduji že s vysokou pravděpodobností si radši napíšete vlastní. Je tu pro inspiraci a je plně funkční.
