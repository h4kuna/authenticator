<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Test,
	Tester\Assert;

$container = require __DIR__ . '/../../bootstrap.php';

class LoginFormTest extends \Tester\TestCase
{

	/** @var LoginForm */
	private $loginForm;

	/** @var Test\UserModel */
	private $userModel;

	/** @var int */
	private $userId;

	public function __construct(LoginForm $loginForm, Test\UserModel $userModel, $userId)
	{
		$this->loginForm = $loginForm;
		$this->userModel = $userModel;
		$this->userId = $userId;
	}

	public function testHtml()
	{
		Assert::same(file_get_contents(__DIR__ . '/loginForm-html.html'), (string) $this->loginForm->create());
	}

	/**
	 * @dataProvider loginForm-send.ini
	 */
	public function testSend($username, $password, $result)
	{
		if ($result == 2) {
			$this->userModel->updateUser($this->userId, ['block' => 1]);
		}
		$counter = new Test\Counter();
		$this->loginForm->onIdentityNotFound[] = function() use ($counter) {
			$counter->count += 1;
		};
		$this->loginForm->onIdentityIsBlocked[] = function() use ($counter) {
			$counter->count += 2;
		};
		$this->loginForm->onInvalidPassword[] = function() use ($counter) {
			$counter->count += 3;
		};
		$this->loginForm->onSuccess[] = function() use ($counter) {
			$counter->count += 4;
		};
		$this->loginForm->onCatchException[] = function ($values, $e) use ($counter) {
			Assert::true($e instanceof Test\MyTestException);
			$counter->count += 5;
		};
		$form = $this->loginForm->create();
		$form->send([
			'username' => $username,
			'password' => $password
		]);
		Assert::same((int) $result, $counter->count);
		if ($result == 2) {
			$this->userModel->updateUser($this->userId, ['block' => 0]);
		}
	}

	public function testSendEmail()
	{
		$this->loginForm->nicknameIsEmail();
		$form = $this->loginForm->create();
		Assert::same(['Vložte validní email.'], $form->send([
				'username' => 'foo',
				'password' => 'bar'
		]));
	}

}

$loginForm = $container->getService('cms.loginForm');

$userModel = $container->getService('userModel');
$userId = $userModel->createTable()->createDefaultValue();

(new LoginFormTest($loginForm, $userModel, $userId))->run();
