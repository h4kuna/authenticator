<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Test,
	Tester\Assert;

$container = require __DIR__ . '/../../bootstrap.php';

class RegistrationFormTest extends \Tester\TestCase
{

	/** @var RegistrationForm */
	private $registrationForm;

	/** @var Test\UserModel */
	private $userModel;

	public function __construct(RegistrationForm $registrationForm, Test\UserModel $userModel)
	{
		$this->registrationForm = $registrationForm;
		$this->userModel = $userModel;
	}

	public function testHtml()
	{
		file_put_contents('registrationForm-html.html', (string) $this->registrationForm->create());
		Assert::same(file_get_contents('registrationForm-html.html'), (string) $this->registrationForm->create());
	}

	public function testRegister()
	{
		$counter = new Test\Counter();
		$this->registrationForm->onSuccess[] = function($hash, $userData) use ($counter) {
			$counter->count = 1;
		};
		$form = $this->registrationForm->create();
		$form->send([
			'email' => 'doe.joe@gmail.com',
			'username' => 'jack',
			'password' => [
				'original' => 'password',
				'copy' => 'password'
			]
		]);

		Assert::same($counter->count, 1);
		$this->uniqueEmail();
		$this->uniqueUsername();
	}

	private function uniqueEmail()
	{
		// unique email
		$form = $this->registrationForm->create();
		Assert::same(['E-mail je již zaregistrovaný, zvolte jiný nebo si nechte obnovit heslo.'], $form->send([
				'email' => 'doe.joe@gmail.com',
				'username' => '',
				'password' => [
					'original' => 'password',
					'copy' => 'password'
				]
		]));
	}

	private function uniqueUsername()
	{
		// unique username
		$form = $this->registrationForm->create();
		Assert::same(['Uživatelské jméno je již obsazené. Zvolte jiné.'], $form->send([
				'email' => 'foo@gmail.com',
				'username' => 'jack',
				'password' => [
					'original' => 'password',
					'copy' => 'password'
				]
		]));
	}

	public function testInvalidEmail()
	{
		$form = $this->registrationForm->create();
		Assert::same(['Vložte validní email.'], $form->send([
				'email' => 'doe.joe@gmail',
				'password' => [
					'original' => 'password',
					'copy' => 'password'
				]
		]));
	}

	public function testInvalidPasswordMinLength()
	{
		$form = $this->registrationForm->create();
		Assert::same(['Minimální délku hesla zvolte 7 znaků.'], $form->send([
				'email' => 'doe.joe@gmail2.com',
				'password' => [
					'original' => 'p',
					'copy' => 'p'
				]
		]));
	}

	public function testInvalidPasswordsNotSame()
	{
		$form = $this->registrationForm->create();
		Assert::same(['Hesla se neshodují.'], $form->send([
				'email' => 'doe.joe@gmail2.com',
				'password' => [
					'original' => 'password',
					'copy' => 'password2'
				]
		]));
	}

	public function testInvalidNotFilled()
	{
		$form = $this->registrationForm->create();
		Assert::same(['Zadejte prosím heslo ještě jednou pro kontrolu.'], $form->send([
				'email' => 'doe.joe@gmail2.com',
				'password' => [
					'original' => 'password',
					'copy' => ''
				]
		]));
	}

}

$registrationForm = $container->getService('cms.registrationForm');

$userModel = $container->getService('userModel');
$userModel->createTable();

(new RegistrationFormTest($registrationForm, $userModel))->run();
