<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Core,
	h4kuna\Cms\Test,
	Tester\Assert;

$container = require __DIR__ . '/../../bootstrap.php';

class RestorePasswordFormTest extends \Tester\TestCase
{

	/** @var RestorePasswordForm */
	private $restorePasswordForm;

	/** @var Test\UserModel */
	private $userModel;

	/** @var Core\Security\Authenticator */
	private $authenticator;

	/** @var int */
	private $userId;

	/** @var string */
	private $hash;

	public function __construct(RestorePasswordForm $restorePasswordForm, Test\UserModel $userModel, Core\Security\Authenticator $authenticator, $userId)
	{
		$this->restorePasswordForm = $restorePasswordForm;
		$this->userModel = $userModel;
		$this->authenticator = $authenticator;
		$this->userId = $userId;
	}

	public function setUp()
	{
		$userData = $this->userModel->fetchUserById($this->userId);
		$this->hash = '05423105f6ed323e3f7375a34c42312b';
		$this->userModel->restorePassword($this->hash, $userData);
	}

	public function testHtml()
	{
		// file_put_contents('restorePasswordForm-html.html', (string) $this->restorePasswordForm->create($this->hash));
		Assert::same(file_get_contents('restorePasswordForm-html.html'), (string) $this->restorePasswordForm->create($this->hash));
	}

	public function testSend()
	{
		$counter = new Test\Counter();
		$restore = $this->restorePasswordForm->create($this->hash);
		$this->restorePasswordForm->onFail[] = function() use ($counter) {
			$counter->count += 1;
		};
		$this->restorePasswordForm->onSuccess[] = function () use ($counter) {
			$counter->count += 2;
		};
		$password = '1234567';
		Assert::same([], $restore->send([
				'password' => [
					'original' => $password,
					'copy' => $password
				]
		]));

		// login
		$userData = $this->userModel->fetchUserById($this->userId);

		$user = $this->authenticator->loginByPassword($userData->email, $password);
		Assert::same(2, $counter->count);
		Assert::true($user->isLoggedIn());
	}

	public function testSendFaild()
	{
		$counter = new Test\Counter();
		$restore = $this->restorePasswordForm->create('failtoken123456');
		$this->restorePasswordForm->onFail[] = function() use ($counter) {
			$counter->count += 1;
		};
		$this->restorePasswordForm->onSuccess[] = function () use ($counter) {
			$counter->count += 2;
		};
		$password = '123456a';
		$restore->send([
			'password' => [
				'original' => $password,
				'copy' => $password
			]
		]);

		// login
		$userData = $this->userModel->fetchUserById($this->userId);

		Assert::exception(function() use ($userData, $password) {
			$this->authenticator->loginByPassword($userData->email, $password);
		}, 'h4kuna\Cms\InvalidPasswordException');

		Assert::same(1, $counter->count);
	}

}

$restorePasswordForm = $container->getService('cms.restorePasswordForm');

$userModel = $container->getService('userModel');
$authenticator = $container->getService('cms.authenticator');
$userId = $userModel->createTable()->createDefaultValue();

(new RestorePasswordFormTest($restorePasswordForm, $userModel, $authenticator, $userId))->run();
