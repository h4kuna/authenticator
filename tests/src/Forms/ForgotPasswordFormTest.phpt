<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Test,
	Tester\Assert;

$container = require __DIR__ . '/../../bootstrap.php';

class ForgotPasswordFormTest extends \Tester\TestCase
{

	/** @var ForgotPasswordForm */
	private $forgotPasswordForm;

	/** @var Test\UserModel */
	private $userModel;

	public function __construct(ForgotPasswordForm $forgotPasswordForm, Test\UserModel $userModel)
	{
		$this->forgotPasswordForm = $forgotPasswordForm;
		$this->userModel = $userModel;
	}

	public function testHtml()
	{
		Assert::same(file_get_contents(__DIR__ . '/forgotPasswordForm-html.html'), (string) $this->forgotPasswordForm->create());
	}

	/**
	 * @dataProvider forgotPassword-send.ini
	 */
	public function testSend($username, $expect)
	{
		$counter = new Test\Counter();
		$form = $this->forgotPasswordForm->create();
		$this->forgotPasswordForm->onSuccess[] = function ($hash, $data) use ($counter) {
			Assert::same($this->userModel->getRestoreData($data['id'])['hash'], $hash);
			$counter->count += 1;
		};
		$this->forgotPasswordForm->onUsernameNotFound[] = function () use ($counter) {
			$counter->count += 2;
		};
		$form->send([
			'username' => $username
		]);
		Assert::same((int) $expect, $counter->count);
	}

	public function testValidEmail()
	{
		$this->forgotPasswordForm->nicknameIsEmail();
		$form = $this->forgotPasswordForm->create();
		Assert::same(['Vložte validní email.'], $form->send([
				'username' => 'foo',
				'password' => 'bar'
		]));
	}

}

$forgotPasswordForm = $container->getService('cms.forgotPasswordForm');

$userModel = $container->getService('userModel');
$userModel->createTable()->createDefaultValue();

(new ForgotPasswordFormTest($forgotPasswordForm, $userModel))->run();
