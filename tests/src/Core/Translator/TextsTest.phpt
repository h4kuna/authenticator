<?php

namespace h4kuna\Cms\Core\Translator;

use Tester\Assert;

$container = require __DIR__ . '/../../../bootstrap.php';

class TextsTest extends \Tester\TestCase
{

	/** @var Texts */
	private $texts;

	public function __construct(Texts $texts)
	{
		$this->texts = $texts;
	}

	public function testBasic()
	{
		Assert::same($this->texts->translate('form.login.username.label'), 'Nick');
		Assert::same($this->texts->translate('form.fields.valid.email'), 'Vložte validní email.');
	}

}

(new TextsTest($container->getService('cms.texts')))->run();
