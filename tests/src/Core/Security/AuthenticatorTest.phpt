<?php

namespace h4kuna\Cms\Core\Security;

use h4kuna\Cms\Test,
	Nette\Security,
	Tester\Assert;

$container = require __DIR__ . '/../../../bootstrap.php';

class AuthenticatorTest extends \Tester\TestCase
{

	/** @var Authenticator */
	private $authenticator;

	/** @var int */
	private $userId;

	/** @var Test\UserModel */
	private $userModel;

	/** @var Security\User */
	private $user;

	public function __construct(Authenticator $authenticator, $userId, Test\UserModel $userModel, Security\User $user)
	{
		$this->authenticator = $authenticator;
		$this->userId = $userId;
		$this->userModel = $userModel;
		$this->user = $user;
	}

	protected function tearDown()
	{
		$this->userModel->updateUser($this->userId, ['block' => 0]);
		$this->user->logout(TRUE);
	}

	public function testLoginById()
	{
		$user = $this->authenticator->loginById($this->userId);
		Assert::false(isset($user->getIdentity()->data['password']));
		Assert::false(isset($user->getIdentity()->data['block']));
		Assert::true(isset($user->getIdentity()->data['id']));
		Assert::same($this->userId, $user->getId());
		Assert::true($user->isLoggedIn());
		Assert::same(1, $this->userModel->fetchUserById($this->userId)->login_count);
	}

	public function testLoginByPassword()
	{
		$user = $this->authenticator->loginByPassword('milan.matejcek@gmail.com', 'password');
		Assert::same($this->userId, $user->getId());
		Assert::true($user->isLoggedIn());
	}

	/**
	 * @throws \h4kuna\Cms\InvalidPasswordException
	 */
	public function testLoginByPasswordFailPassword()
	{
		$this->authenticator->loginByPassword('milan.matejcek@gmail.com', 'heslo');
	}

	/**
	 * @throws \h4kuna\Cms\IdentityNotFoundException
	 */
	public function testLoginByPasswordFailIdentity()
	{
		$this->authenticator->loginByPassword('doe.joe@gmail.com', 'password');
	}

	/**
	 * @throws \h4kuna\Cms\IdentityIsBlockedException
	 */
	public function testLoginByPasswordFaildBlocked()
	{
		$this->userModel->updateUser($this->userId, ['block' => 1]);
		$this->authenticator->loginByPassword('milan.matejcek@gmail.com', 'password');
	}

	/**
	 * @throws \h4kuna\Cms\InvalidPasswordException
	 */
	public function testEmptyPassword()
	{
		$oldPassword = $this->userModel->setPassword($this->userId, '');
		try {
			$this->authenticator->loginByPassword('milan.matejcek@gmail.com', '');
		} catch (\h4kuna\Cms\AuthenticatorException $e) {
			$this->userModel->setPassword($this->userId, $oldPassword);
			throw $e;
		}
	}

	public function testReloadIdentityBasic()
	{
		Assert::false($this->authenticator->reloadIdentity(new Security\Identity(NULL)));
		$user = $this->authenticator->loginById($this->userId);
		Assert::null($user->getIdentity()->data['name']);
		$this->userModel->updateUser($this->userId, ['name' => 'Doe']);
		$this->authenticator->reloadIdentity($user->getIdentity());
		Assert::same('Doe', $user->getIdentity()->data['name']);
	}

	public function testReloadIdentityEvents()
	{
		$counter = new Test\Counter();
		$this->user->onLoggedIn[] = function() use ($counter) {
			++$counter->count;
		};
		$this->user->onLoggedOut[] = function() use ($counter) {
			++$counter->count;
		};
		$user = $this->authenticator->loginById($this->userId);
		$this->userModel->updateUser($this->userId, ['name' => 'Doe2']);
		$this->authenticator->reloadIdentity($user->getIdentity());
		Assert::same('Doe2', $user->getIdentity()->data['name']);
		Assert::same(1, $counter->count);

		$this->userModel->updateUser($this->userId, ['name' => 'Doe3']);
		$this->authenticator->reloadIdentity($user->getIdentity(), FALSE);
		Assert::same('Doe3', $user->getIdentity()->data['name']);
		Assert::same(3, $counter->count);
	}

	/**
	 * @dataProvider authenticator-reloadIdentityBlockUser.ini
	 */
	public function testReloadIdentityBlockUser($expect, $event)
	{
		$counter = new Test\Counter();
		$this->user->onLoggedIn[] = function() use ($counter) {
			++$counter->count;
		};
		$this->user->onLoggedOut[] = function() use ($counter) {
			++$counter->count;
		};
		$user = $this->authenticator->loginById($this->userId);
		$this->userModel->updateUser($this->userId, ['block' => 1]);
		$this->authenticator->reloadIdentity($user->getIdentity(), (bool) $event);
		Assert::false($user->isLoggedIn());
		Assert::same((int) $expect, $counter->count);
	}

}

$userModel = $container->getService('userModel');
$user = $container->getByType('Nette\Security\User');
$userId = $userModel->createTable()->createDefaultValue();

(new AuthenticatorTest($container->getService('cms.authenticator'), (int) $userId, $userModel, $user))->run();
