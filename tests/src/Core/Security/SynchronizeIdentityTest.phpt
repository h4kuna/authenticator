<?php

namespace h4kuna\Cms\Core\Security;

use h4kuna\Cms\Test,
	Nette\Security,
	Tester\Assert;

$container = require __DIR__ . '/../../../bootstrap.php';

class SynchronizeIdentityTest extends \Tester\TestCase
{

	/** @var Authenticator */
	private $authenticator;

	/** @var Security\User */
	private $user;

	/** @var SynchronizeIdentity */
	private $synchronizeIdentity;

	/** @var SynchronizeIdentityChecker */
	private $synchronizeIdentityChecker;

	/** @var Test\UserModel */
	private $userModel;

	/** @var int */
	private $userId;

	public function __construct(Authenticator $authenticator, Security\User $user, SynchronizeIdentity $synchronizeIdentity, SynchronizeIdentityChecker $synchronizeIdentityChecker, Test\UserModel $userModel, $userId)
	{
		$this->authenticator = $authenticator;
		$this->user = $user;
		$this->synchronizeIdentity = $synchronizeIdentity;
		$this->synchronizeIdentityChecker = $synchronizeIdentityChecker;
		$this->userModel = $userModel;
		$this->userId = $userId;
	}

	protected function tearDown()
	{
		$this->user->logout();
		$this->userModel->updateUser($this->userId, ['name' => NULL]);
	}

	public function testCheckerMyToken()
	{
		$user = $this->authenticator->loginById($this->userId);
		Assert::null($user->getIdentity()->name);
		$this->userModel->updateUser($this->userId, ['name' => 'foo']);
		$this->synchronizeIdentity->reloadUserToken($this->userId);
		Assert::null($user->getIdentity()->name);
		$this->synchronizeIdentityChecker->run();
		Assert::same('foo', $user->getIdentity()->name);
	}

	public function testCheckerGlobalToken()
	{
		$user = $this->authenticator->loginById($this->userId);
		Assert::null($user->getIdentity()->name);
		$this->userModel->updateUser($this->userId, ['name' => 'foo']);
		$this->synchronizeIdentity->realodGlobalToken();
		$this->synchronizeIdentityChecker->run();
		Assert::same('foo', $user->getIdentity()->name);
	}

	public function testReloadLoggedUser()
	{
		$user = $this->authenticator->loginById($this->userId);
		Assert::null($user->getIdentity()->name);
		$this->userModel->updateUser($this->userId, ['name' => 'foo']);
		$this->synchronizeIdentity->reloadLoggedUser();
		Assert::same('foo', $user->getIdentity()->name);
	}

}

$authenticator = $container->getService('cms.authenticator');
$user = $container->getByType('Nette\Security\User');
$synchronizeIdentity = $container->getService('cms.synchronizeIdentity');
$synchronizeIdentityChecker = $container->getService('cms.synchronizeIdentityChecker');

$userModel = $container->getService('userModel');
$userId = $userModel->createTable()->createDefaultValue();

(new SynchronizeIdentityTest($authenticator, $user, $synchronizeIdentity, $synchronizeIdentityChecker, $userModel, (int) $userId))->run();
