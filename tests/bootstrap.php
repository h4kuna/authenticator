<?php

require_once __DIR__ . "/../vendor/autoload.php";
require __DIR__ . '/libs/ConfiguratorFactory.php';

$tempDir = __DIR__ . '/temp';

if (!getenv('KEEP_TEMP')) {
	exec("rm -r $tempDir/*");
}

$configurator = h4kuna\Cms\Test\ConfiguratorFactory::create($tempDir, __DIR__ . '/libs/');
$configurator->addConfig(__DIR__ . '/config/config.neon');

Tester\Environment::setup();



return $configurator->createContainer();
