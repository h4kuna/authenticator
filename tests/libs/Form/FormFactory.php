<?php

namespace h4kuna\Cms\Test\Form;

class FormFactory extends \h4kuna\Cms\Core\Form\FormFactory
{

	protected function createForm()
	{
		return new Form();
	}

}
