<?php

namespace h4kuna\Cms\Test\Form;

class Form extends \Nette\Application\UI\Form
{

	private $send = FALSE;
	private $data;

	public function isAnchored()
	{
		return $this->send;
	}

	protected function receiveHttpData()
	{
		return $this->data;
	}

	public function send(array $data = array(), $button = NULL)
	{
		$this->cleanErrors();
		$this->setValues($this->data = $data);
		if ($button) {
			$this->setSubmittedBy($this[$button]);
		}
		$this->send = TRUE;
		$this->fireEvents();
		return $this->getErrors();
	}

}
