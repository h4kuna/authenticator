<?php

namespace h4kuna\Cms\Test;

use Nette\Database,
	h4kuna\Cms\Core\Security,
	Nette\Security as NSecurity,
	Nette\Utils;

class UserModel implements Security\AuthenticatorFacadeInterface
{

	/** @var Database\Context */
	private $context;

	public function __construct(Database\Context $context)
	{
		$this->context = $context;
	}

	public function fetchUserById($id)
	{
		return $this->table()->where('id', $id)->fetch();
	}

	public function fetchUserByUsername($username)
	{
		if ($username === 'exception') {
			throw new MyTestException();
		}
		return $this->table()->where('email = ? OR name = ?', $username, $username)->fetch();
	}

	public function createAuthenticatorStructure($data)
	{
		if ($data) {
			$rawData = $data->toArray();
		} else {
			$rawData = [
				'id' => NULL,
				'block' => NULL,
			];
		}
		$struct = new Security\AuthenticatorStructure($rawData, $rawData['block'], $rawData['id']);
		$struct->setPassword($data['password']);
		return $struct;
	}

	public function loginSuccess(NSecurity\User $user, $method)
	{
		if ($method === 'reload') {
			return;
		}
		$this->table()->where('id', $user->getId())->update(['login_count' => new Database\SqlLiteral('login_count + 1')]);
	}

	public function restorePassword($hash, $userData, $action = 'restore')
	{
		$this->context->table('user_password')->where('user_id', $userData->id)->where('action', 'restore')->delete();

		$this->context->table('user_password')->insert([
			'user_id' => $userData->id,
			'hash' => $hash,
			'insert_dt' => new \DateTime,
			'action' => $action
		]);

		return $userData->toArray();
	}

	public function register($hash, $values)
	{
		$userId = $this->table()->insert([
			'name' => $values->username ? : NULL,
			'password' => $values->password,
			'email' => $values->email
		]);

		$userData = $this->fetchUserById($userId);
		return $this->restorePassword($hash, $userData, 'registration');
	}

	public function isEmailUnique($email)
	{
		return !$this->table()->where('email', $email)->fetch();
	}

	public function isUsernameUnique($username)
	{
		return !$this->table()->where('name', $username)->fetch();
	}

	public function updatePassword($hash, $password)
	{
		$data = $this->confirmHash($hash);
		if (!$data) {
			return FALSE;
		}
		$this->table()->where('id', $data->user_id)->update([
			'password' => $password
		]);
		return TRUE;
	}

	private function table($table = 'user')
	{
		return $this->context->table($table);
	}

	/**
	 * HELPER METHODS **********************************************************
	 */
	public function createTable()
	{
		$this->context->getConnection()->query('CREATE TABLE IF NOT EXISTS user (' .
			'id INTEGER PRIMARY KEY AUTOINCREMENT,' .
			'email text NOT NULL,' .
			'password text NOT NULL,' .
			'name text NULL,' .
			'block integer NOT NULL DEFAULT 0, ' .
			'login_count int NOT NULL DEFAULT 0,' .
			'column_remove int NULL' .
			');');


		$this->context->getConnection()->query('CREATE TABLE IF NOT EXISTS user_password (' .
			'user_id int NOT NULL,' .
			'hash text NOT NULL,' .
			'action text NOT NULL DEFAULT \'restore\', ' .
			'insert_dt text NOT NULL' .
			');');
		return $this;
	}

	public function getRestoreData($userId)
	{
		return $this->context->table('user_password')->where('user_id', $userId)->fetch()->toArray();
	}

	public function createDefaultValue()
	{
		return $this->context->table('user')->insert([
				'email' => 'milan.matejcek@gmail.com',
				'password' => NSecurity\Passwords::hash('password')
			])->id;
	}

	public function setPassword($userId, $password)
	{
		$userData = $this->context->table('user')->select('password')->where('id', $userId)->fetch();
		$this->updateUser($userId, ['password' => $password]);
		return $userData->password;
	}

	public function updateUser($userId, $data)
	{
		$this->context->table('user')->where('id', $userId)->update($data);
	}

	/** @rerturn ActiveRow|NULL */
	public function confirmHash($hash)
	{
		$data = $this->table('user_password')->where('hash', $hash)->fetch();
		if (!$data) {
			return NULL;
		} elseif (Utils\DateTime::from($data->insert_dt)->modify('+1 day') < new \DateTime('now')) {
			$this->table('user_password')->where('hash', $hash)->delete();
			return NULL;
		}

		if ($data->action === 'restore') {
			$this->table('user_password')->where('user_id', $data->user_id)->delete();
		} else {
			$this->table('user_password')->where('hash', $hash)->delete();
		}
		return $data;
	}

}
