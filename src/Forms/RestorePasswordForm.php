<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Core,
	Nette\Security as NSecurity;

class RestorePasswordForm extends \Nette\Object
{

	/** @var callable[] */
	public $onSuccess;

	/** @var callable[] */
	public $onFail;

	/** @var Core\Form\FormFactory */
	private $formFactory;

	/** @var Core\Translator\Texts */
	private $texts;

	/** @var Core\Security\AuthenticatorFacadeInterface */
	private $authenticateFacade;

	/** @var int */
	private $passwordLength = 6;

	public function __construct(Core\Form\FormFactory $formFactory, Core\Translator\Texts $texts, Core\Security\AuthenticatorFacadeInterface $authenticateFacade)
	{
		$this->formFactory = $formFactory;
		$this->texts = $texts;
		$this->authenticateFacade = $authenticateFacade;
	}

	public function setPasswordLength($passwordLength)
	{
		$this->passwordLength = (int) $passwordLength;
	}

	public function create($hash)
	{
		$form = $this->formFactory->create();
		$form->addHidden('hash', $hash);
		$password = $form->addContainer('password');
		$this->formFactory->getFields()->addPassword($password, $this->passwordLength);
		$password['original']->setRequired($this->texts->translate('form.fields.password.original.filled'));
		$password['copy']->setRequired($this->texts->translate('form.fields.password.copy.filled'));

		$form->addSubmit('send', $this->texts->translate('form.restore.password.submit'));
		$form->onSuccess[] = [$this, 'formSuccess'];
		return $form;
	}

	public function formSuccess($form, $values)
	{
		$password = NSecurity\Passwords::hash($values->password->original);
		if ($this->authenticateFacade->updatePassword($values->hash, $password)) {
			$this->onSuccess();
		} else {
			$this->onFail();
		}
	}

}
