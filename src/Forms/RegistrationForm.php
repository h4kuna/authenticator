<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Core,
	Nette\Security,
	Nette\Utils;

class RegistrationForm extends \Nette\Object
{

	/** @var callable[] */
	public $onSuccess;

	/** @var Core\Form\FormFactory */
	private $formFactory;

	/** @var Core\Security\AuthenticatorFacadeInterface */
	private $authenticatorFacade;

	/** @var Core\Translator\TextsInterface */
	private $texts;

	/** @var int[] */
	private $minLength = [
		'username' => 3,
		'password' => 6
	];

	public function __construct(Core\Form\FormFactory $formFactory, Core\Security\AuthenticatorFacadeInterface $authenticatorFacade, Core\Translator\TextsInterface $texts)
	{
		$this->formFactory = $formFactory;
		$this->authenticatorFacade = $authenticatorFacade;
		$this->texts = $texts;
	}

	public function setMinLength($username, $password)
	{
		$this->minLength = [
			'username' => $username,
			'password' => $password
		];
		return $this;
	}

	public function create()
	{
		$form = $this->formFactory->create();
		$fields = $this->formFactory->getFields();

		$form->addText('username', $this->texts->translate('form.registration.username.label'))
			->addCondition($form::FILLED)
			->addRule($form::MIN_LENGTH, $this->texts->translate('form.registration.username.min.length'), $this->minLength['username'])
			->addRule(function ($input) {
				return $this->isUsernameUnique($input->getValue());
			}, $this->texts->translate('form.registration.username.not.unique'));

		$form['email'] = $fields->addEmail($this->texts->translate('form.registration.email.label'))
			->setRequired($this->texts->translate('form.registration.email.required'))
			->addRule(function($input) {
			return $this->isEmailUnique($input->getValue());
		}, $this->texts->translate('form.registration.email.not.unique'));

		$password = $fields->addPassword($form->addContainer('password'), $this->minLength['password']);
		$password['original']->setRequired($this->texts->translate('form.fields.password.original.filled'));
		$password['copy']->setRequired($this->texts->translate('form.fields.password.copy.filled'));
		$form->addSubmit('send', $this->texts->translate('form.registration.submit'));
		$form->onSuccess[] = [$this, 'formSuccess'];
		return $form;
	}

	public function formSuccess($form, $values)
	{
		$values['password'] = Security\Passwords::hash($values['password']['original']);
		$hash = Core\Utils\Strings::hash($values->email);
		$data = $this->authenticatorFacade->register($hash, $values);
		$this->onSuccess($hash, $data);
	}

	public function isEmailUnique($email)
	{
		return $this->authenticatorFacade->isEmailUnique(Utils\Strings::lower($email));
	}

	public function isUsernameUnique($username)
	{
		return $this->authenticatorFacade->isUsernameUnique(Utils\Strings::lower($username));
	}

}
