<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms,
	h4kuna\Cms\Core\Form,
	h4kuna\Cms\Core\Security,
	h4kuna\Cms\Core\Translator;

class LoginForm extends \Nette\Object
{

	/** @var callable[] */
	public $onSuccess;

	/** @var callable[] */
	public $onIdentityNotFound;

	/** @var callable[] */
	public $onInvalidPassword;

	/** @var callable[] */
	public $onIdentityIsBlocked;

	/** callable[] */
	public $onCatchException;

	/** @var Form\FormFactory */
	private $formFactory;

	/** @var Security\Authenticator */
	private $authenticator;

	/** @var Translator\TextsInterface */
	private $texts;

	/** @var bool */
	private $emailOnly = FALSE;

	public function __construct(Form\FormFactory $formFactory, Security\Authenticator $authenticator, Translator\TextsInterface $texts)
	{
		$this->formFactory = $formFactory;
		$this->authenticator = $authenticator;
		$this->texts = $texts;
	}

	public function nicknameIsEmail()
	{
		$this->emailOnly = TRUE;
	}

	public function create()
	{
		$form = $this->formFactory->create();

		$username = $form->addText('username', $this->texts->translate('form.login.username.label'));
		if ($this->emailOnly) {
			$username->addRule($form::EMAIL, $this->texts->translate('form.fields.valid.email'));
		}
		$username->isRequired($this->texts->translate('form.login.usenrame.filled'));
		$form->addPassword('password', $this->texts->translate('form.login.password'));
		$form->addCheckbox('remember', $this->texts->translate('form.login.remember'));
		$form->addSubmit('login', $this->texts->translate('form.login.submit'));
		$form->onSuccess[] = [$this, 'formSuccess'];
		return $form;
	}

	public function formSuccess($form, $values)
	{
		try {
			$user = $this->authenticator->loginByPassword($values->username, $values->password);
		} catch (Cms\IdentityNotFoundException $e) {
			return $this->onIdentityNotFound($values);
		} catch (Cms\InvalidPasswordException $e) {
			return $this->onInvalidPassword($values);

		} catch (Cms\IdentityIsBlockedException $e) {
			return $this->onIdentityIsBlocked($values);
		} catch (\Exception $e) {
			if (!$this->onCatchException) {
				throw $e;
			}
			return $this->onCatchException($values, $e);
		}
		$this->onSuccess($user, $values->remember);
	}

}
