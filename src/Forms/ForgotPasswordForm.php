<?php

namespace h4kuna\Cms\Forms;

use h4kuna\Cms\Core;

class ForgotPasswordForm extends \Nette\Object
{

	/** @var callable[] */
	public $onUsernameNotFound;

	/** @var callable[] */
	public $onSuccess;

	/** @var Core\Form\FormFactory */
	private $formFactory;

	/** @var LoginForm */
	private $loginForm;

	/** @var Core\Translator\TextsInterface */
	private $texts;

	/** @var Core\Security\AuthenticatorFacadeInterface */
	private $authenticatorFacade;

	/** @var bool */
	private $emailOnly = FALSE;

	public function __construct(Core\Form\FormFactory $formFactory, LoginForm $loginForm, Core\Translator\TextsInterface $texts, Core\Security\AuthenticatorFacadeInterface $authenticatorFacade)
	{
		$this->formFactory = $formFactory;
		$this->loginForm = $loginForm;
		$this->texts = $texts;
		$this->authenticatorFacade = $authenticatorFacade;
	}

	public function nicknameIsEmail()
	{
		$this->emailOnly = TRUE;
	}

	public function create()
	{
		$form = $this->formFactory->create();

		$username = $form->addText('username', $this->texts->translate('form.forgot.username.label'));
		if ($this->emailOnly) {
			$username->addRule($form::EMAIL, $this->texts->translate('form.fields.valid.email'));
		}

		$username->setRequired($this->texts->translate('form.forgot.username.filled'));

		$form->addSubmit('send', $this->texts->translate('form.forgot.submit'));
		$form->onSuccess[] = [$this, 'formSuccess'];
		return $form;
	}

	public function formSuccess($form, $values)
	{
		$userData = $this->authenticatorFacade->fetchUserByUsername($values->username);
		if (!$userData) {
			$this->onUsernameNotFound($values->username);
			return;
		}
		$hash = Core\Utils\Strings::hash($values->username);
		$data = $this->authenticatorFacade->restorePassword($hash, $userData);
		$this->onSuccess($hash, $data);
	}

}
