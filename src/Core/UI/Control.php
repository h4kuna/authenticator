<?php

namespace h4kuna\Cms\Core\UI;

use Nette\Application;

abstract class Control extends Application\UI\Control
{

	/** @var callable */
	private $onCreateTemplate;

	/** @var bool */
	private $templateExists = FALSE;

	public function setFile($file)
	{
		if ($this->templateExists) {
			$this->getTemplate()->setFile($file);
		} else {
			$this->onCreateTemplate = function($template) use ($file) {
				$template->setFile($file);
			};
		}
	}

	protected function createTemplate()
	{
		$this->templateExists = TRUE;
		$template = parent::createTemplate();
		if ($this->onCreateTemplate) {
			call_user_func($this->onCreateTemplate, $template);
		}
		return $template;
	}

	public function render()
	{
		$template = $this->getTemplate();
		$this->initTemplate($template);
		$template->render();
	}

	protected function initTemplate($template)
	{
	}

}
