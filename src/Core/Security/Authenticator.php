<?php

namespace h4kuna\Cms\Core\Security;

use h4kuna\Cms,
	Nette\Security AS NSecurity;

class Authenticator
{

	/** @var callable */
	public $onCreateIdentity;

	/** @var IdentityFactory */
	protected $identityFactory;

	/** @var AuthenticatorFacadeInterface */
	protected $authenticatorFacade;

	/** @var NSecurity\User */
	protected $user;

	public function __construct(IdentityFactory $identityFactory, AuthenticatorFacadeInterface $authenticatorFacade, NSecurity\User $user)
	{
		$this->identityFactory = $identityFactory;
		$this->authenticatorFacade = $authenticatorFacade;
		$this->user = $user;
	}

	/**
	 * @param mixed $id
	 * @return NSecurity\User
	 * @throws Cms\IdentityNotFoundException
	 * @throws Cms\IdentityIsBlockedException
	 */
	public function loginById($id)
	{
		$data = $this->authenticatorFacade->fetchUserById($id);
		return $this->login($this->authenticatorFacade->createAuthenticatorStructure($data), 'id');
	}

	/**
	 * @param mixed $username
	 * @param string $password
	 * @return NSecurity\User
	 * @throws Cms\IdentityNotFoundException
	 * @throws Cms\InvalidPasswordException
	 * @throws Cms\IdentityIsBlockedException
	 */
	public function loginByPassword($username, $password)
	{
		$rawData = $this->authenticatorFacade->fetchUserByUsername($username);
		$data = $this->authenticatorFacade->createAuthenticatorStructure($rawData);
		$this->checkIdentity($data);
		if (!NSecurity\Passwords::verify($password, $data->getPassword())) {
			throw new Cms\InvalidPasswordException();
		}
		return $this->login($data, 'password');
	}

	/**
	 * @param NSecurity\Identity $identity
	 * @param bool $removeEvents
	 * @return bool
	 */
	public function reloadIdentity(NSecurity\Identity $identity, $removeEvents = TRUE)
	{
		if (!$identity->getId()) {
			return FALSE;
		}
		if ($removeEvents) {
			$onLoggedOut = $this->user->onLoggedOut;
			$onLoggedIn = $this->user->onLoggedIn;
			$this->user->onLoggedIn = $this->user->onLoggedOut = [];
		}
		$rawData = $this->authenticatorFacade->fetchUserById($identity->getId());
		try {
			$this->login($this->authenticatorFacade->createAuthenticatorStructure($rawData), 'reload');
			if ($removeEvents) {
				$this->user->onLoggedIn = $onLoggedIn;
				$this->user->onLoggedOut = $onLoggedOut;
			}
		} catch (Cms\AuthenticatorException $e) {
			$this->user->logout(TRUE);
			return FALSE;
		}
		return TRUE;
	}

	protected function login(AuthenticatorStructure $data, $method)
	{
		$this->checkIdentity($data);
		if ($data->isBlocked()) {
			throw new Cms\IdentityIsBlockedException($data->getId());
		}
		$identity = $this->identityFactory->create($data);
		$this->user->login($identity);
		$this->authenticatorFacade->loginSuccess($this->user, $method);
		return $this->user;
	}

	/**
	 * @param AuthenticatorStructure $data
	 * @throws Cms\IdentityNotFoundException
	 */
	protected function checkIdentity(AuthenticatorStructure $data)
	{
		if (!$data->getId()) {
			throw new Cms\IdentityNotFoundException();
		}
	}

}
