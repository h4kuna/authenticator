<?php

namespace h4kuna\Cms\Core\Security;

use Nette\Security AS NSecurity;

class IdentityFactory
{

	/** @var SynchronizeIdentity */
	private $synchronizeIdentity;

	/** @var array */
	private $removeColumns;

	public function __construct(SynchronizeIdentity $synchronizeIdentity, array $configs)
	{
		$this->synchronizeIdentity = $synchronizeIdentity;
		$this->removeColumns = $configs;
	}

	/**
	 * @param AuthenticatorStructure $data
	 * @return NSecurity\Identity
	 */
	public function create(AuthenticatorStructure $data)
	{
		$dataRaw = $data->getData();
		foreach ($this->removeColumns as $column) {
			unset($dataRaw[$column]);
		}

		$dataRaw[$this->synchronizeIdentity->getSyncKey()] = $this->synchronizeIdentity->getTokens($data->getId());
		return new NSecurity\Identity($data->getId(), $data->getRole(), $dataRaw);
	}

}
