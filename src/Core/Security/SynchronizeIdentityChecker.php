<?php

namespace h4kuna\Cms\Core\Security;

use Nette\Security;

class SynchronizeIdentityChecker
{

	/** @var Security\User */
	private $user;

	/** @var SynchronizeIdentity */
	private $synchronizeIdentity;

	public function __construct(Security\User $user, SynchronizeIdentity $synchronizeIdentity)
	{
		$this->user = $user;
		$this->synchronizeIdentity = $synchronizeIdentity;
	}

	public function run()
	{
		if (!$this->user->isLoggedIn()) {
			return;
		}
		$userTokens = $this->user->getIdentity()->data[$this->synchronizeIdentity->getSyncKey()];
		$storedTokens = $this->synchronizeIdentity->getTokens($this->user->getId());

		if ($userTokens !== $storedTokens) {
			$this->synchronizeIdentity->reloadLoggedUser(FALSE);
		}
	}

}
