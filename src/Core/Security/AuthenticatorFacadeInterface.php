<?php

namespace h4kuna\Cms\Core\Security;

use Nette\Security as NSecurity,
	Nette\Utils;

interface AuthenticatorFacadeInterface
{

	/**
	 * @param string|int $id
	 * @return mixed - your favorite structure
	 */
	function fetchUserById($id);

	/**
	 * @param string $username
	 * @return mixed - your favorite structure
	 */
	function fetchUserByUsername($username);

	/**
	 * @param NSecurity\User $user
	 * @param string $method - is logged by [password, id, reload]
	 */
	function loginSuccess(NSecurity\User $user, $method);

	/**
	 * @param mixed $data - data are from fetch* methods
	 * @return AuthenticatorStructure
	 */
	function createAuthenticatorStructure($data);

	/**
	 * @param string $hash uniq string for identify requirement
	 * @param mixed $userData - data from self::fetchUserByUsername()
	 * @return mixed - this structure is added to success callback for restore password
	 */
	function restorePassword($hash, $userData);

	/**
	 * @param string $hash uniq string for identify requirement
	 * @param Utils\ArrayHash|array $values - data from registration form
	 * @return mixed - add to success callback registration
	 */
	function register($hash, $values);

	/**
	 * @param string $hash
	 * @param string $password
	 * @return bool - TRUE is OK, FALSE mean hash is old or fail
	 */
	function updatePassword($hash, $password);

	/**
	 * @param string $email - value is lower case
	 * @return bool
	 */
	function isEmailUnique($email);

	/**
	 * @param string $username - value is lower case
	 * @return bool
	 */
	function isUsernameUnique($username);
}
