<?php

namespace h4kuna\Cms\Core\Security;

use Nette\Caching,
	Nette\Security as NSecurity;

/**
 * Check identity cross browsers.
 */
class SynchronizeIdentity
{

	private static $globalKey = '-0-';

	/** @var Caching\Cache */
	private $cache;

	/** @var NSecurity\User */
	private $user;

	/** @var Authenticator */
	private $authenticatorAccessor;

	public function __construct(Caching\Cache $cache, NSecurity\User $user, AuthenticatorAccessor $authenticatorAccessor)
	{
		$this->cache = $cache;
		$this->user = $user;
		$this->authenticatorAccessor = $authenticatorAccessor;
	}

	/**
	 * @param bool $crossBrowser
	 * @return bool
	 */
	public function reloadLoggedUser($crossBrowser = TRUE)
	{
		if (!$this->user->isLoggedIn()) {
			return FALSE;
		} elseif ($crossBrowser) {
			$this->reloadUserToken($this->user->getId());
		}
		return $this->authenticatorAccessor->get()->reloadIdentity($this->user->getIdentity());
	}

	public function reloadUserToken($id)
	{
		$this->cache->remove($id);
	}

	public function realodGlobalToken()
	{
		$this->reloadUserToken(self::$globalKey);
	}

	private function getUserToken($id)
	{
		$token = $this->cache->load((string) $id);
		if ($token !== NULL) {
			return $token;
		}

		return $this->cache->save((string) $id, md5(microtime() . '.' . $id));
	}

	private function getGlobalToken()
	{
		return $this->getUserToken(self::$globalKey);
	}

	public function getTokens($id)
	{
		return [
			'my' => $this->getUserToken($id),
			'global' => $this->getGlobalToken()
		];
	}

	public function getSyncKey()
	{
		return '_sync';
	}

}
