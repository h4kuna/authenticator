<?php

namespace h4kuna\Cms\Core\Security;

interface AuthenticatorAccessor
{

	/** @var Authenticator */
	function get();
}
