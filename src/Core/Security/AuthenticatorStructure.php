<?php

namespace h4kuna\Cms\Core\Security;

class AuthenticatorStructure
{

	/** @var array */
	private $data;

	/** @var int|string */
	private $id;

	/** @var array|string */
	private $role;

	/** @var string */
	private $password;

	/** @var mixed */
	private $blocked = TRUE;

	/**
	 *
	 * @param array $data
	 * @param bool $blocked If is TRUE than throw IdentityIsBlockedException on before login.
	 * @param mixed $identificator
	 */
	public function __construct(array $data, $blocked, $identificator)
	{
		$this->data = $data;
		$this->id = $identificator;
		$this->blocked = (bool) $blocked;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getData()
	{
		return $this->data;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function isBlocked()
	{
		return $this->blocked;
	}

	public function setRole($role)
	{
		$this->role = $role;
	}

	/**
	 * Fill password for validation if user is legged in via password.
	 * @param string $password
	 * @return self
	 */
	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}

}
