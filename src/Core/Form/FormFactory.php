<?php

namespace h4kuna\Cms\Core\Form;

use Nette\Application\UI,
	Nette\Forms,
	Nette\Localization,
	h4kuna\Cms\Core\Translator;

class FormFactory
{

	/** @var Translator\TextsInterface */
	private $texts;

	/** @var Fields */
	private $fields;

	/** @var string */
	private $protectionMessage = 'form.protection';

	/** @var Localization\ITranslator */
	private $translator;

	/** @var Forms\IFormRenderer */
	private $renderer;

	public function __construct(Translator\TextsInterface $texts, Fields $fields)
	{
		$this->texts = $texts;
		$this->fields = $fields;
	}

	public function setTranslator(Localization\ITranslator $translator)
	{
		$this->translator = $translator;
	}

	public function setRenderer(Forms\IFormRenderer $renderer)
	{
		$this->renderer = $renderer;
	}

	public function setProtectionMessage($protectionMessage)
	{
		$this->protectionMessage = $protectionMessage;
	}

	/**
	 * @param string|NULL|TRUE $protection
	 * @return UI\Form
	 */
	public function create($protection = FALSE)
	{
		$form = $this->createForm();
		$form->setRenderer($this->renderer)->setTranslator($this->translator);
		if ($protection || $protection === NULL) {
			if ($protection === TRUE) {
				$protection = $this->protectionMessage;
			}
			$form->addProtection($this->texts->translate($protection));
		}
		return $form;
	}

	/** @return Fields */
	public function getFields()
	{
		return $this->fields;
	}

	protected function createForm()
	{
		return new UI\Form;
	}

}
