<?php

namespace h4kuna\Cms\Core\Form;

use h4kuna\Cms\Core,
	Nette\Forms;

class Fields
{

	/** @var Core\Translator\Texts */
	private $texts;

	public function __construct(Core\Translator\Texts $texts)
	{
		$this->texts = $texts;
	}

	public function addPassword(Forms\Container $form, $minLength = 6)
	{
		$original = $form->addPassword('original', $this->texts->translate('form.fields.password.original'))
			->addRule(Forms\Form::MIN_LENGTH, $this->texts->translate('form.fields.password.min.length'), $minLength);
		$form->addPassword('copy', $this->texts->translate('form.fields.password.copy.label'))
			->addRule(Forms\Form::EQUAL, $this->texts->translate('form.fields.password.not.equal'), $original);
		return $form;
	}

	public function addEmail($label)
	{
		$email = new Forms\Controls\TextInput($label, 256);
		$email->addCondition(Forms\Form::FILLED)
			->addRule(Forms\Form::EMAIL, $this->texts->translate('form.fields.valid.email'));
		return $email;
	}

}
