<?php

namespace h4kuna\Cms\Core\Translator;

/**
 * Is only for texts in components.
 */
class Texts implements TextsInterface
{

	/** @var string[] */
	private $dictionary;

	public function __construct($dictionary)
	{
		$this->dictionary = $dictionary;
	}

	public function translate($message)
	{
		if (isset($this->dictionary[$message])) {
			return $this->dictionary[$message];
		}
		return $message;
	}

}
