<?php

namespace h4kuna\Cms\Core\Translator;

interface TextsInterface
{
	function translate($text);
}
