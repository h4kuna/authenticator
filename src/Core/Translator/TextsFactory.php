<?php

namespace h4kuna\Cms\Core\Translator;

use h4kuna\Cms,
	Nette\Caching,
	Nette\Utils;

class TextsFactory
{

	/** @var string[] */
	private $files = [];

	/** @var Caching\Cache */
	private $cache;

	/** @var string */
	private $sourcePath;

	/** @var string[] */
	private $cacheOptions = [];

	/** @var string */
	private $tempArray;

	public function __construct(Caching\Cache $cache)
	{
		$this->cache = $cache;
		$this->cacheOptions = [Caching\Cache::EXPIRATION => '+1 day'];
	}

	public function setSourcePath($sourcePath)
	{
		$this->sourcePath = $sourcePath;
	}

	public function setCacheOptions(array $cacheOptions)
	{
		$this->cacheOptions = $cacheOptions;
	}

	public function addFile($file, $top = FALSE)
	{
		if ($top) {
			array_unshift($this->files, $file);
		} else {
			$this->files[] = $file;
		}
	}

	public function create()
	{
		$dictionary = $this->createDictionary();
		return new Texts($dictionary);
	}

	private function createDictionary()
	{
		$cacheKey = 'dictionary';
		$dictionary = $this->cache->load($cacheKey);
		if ($dictionary !== NULL) {
			return $dictionary;
		}
		$dictionary = [];
		$this->findSourceFiles();

		$duplicity = [];
		foreach ($this->files as $filename) {
			$md5 = md5_file($filename);
			if (isset($duplicity[$md5])) {
				throw new Cms\SourceFileDuplicityException($filename);
			}
			$duplicity[$md5] = TRUE;
			$texts = (new \Nette\DI\Config\Adapters\NeonAdapter())->load($filename);
			foreach ($this->normalizeArray($texts) as $key => $value) {
				$dictionary[$key] = $value;
			}
		}

		return $this->cache->save($cacheKey, $dictionary, $this->cacheOptions);
	}

	private function normalizeArray($texts)
	{
		$this->tempArray = [];
		foreach ($texts as $key => $text) {
			$this->normalizeRow($text, $key);
		}
		return $this->tempArray;
	}

	private function normalizeRow($text, $key)
	{
		if (is_array($text)) {
			foreach ($text as $k => $v) {
				$this->normalizeRow($v, $key . '.' . $k);
			}
		} else {
			$this->tempArray[$key] = $text;
		}
	}

	private function findSourceFiles()
	{
		if (!$this->sourcePath) {
			return;
		}

		foreach (Utils\Finder::findFiles('texts.neon')->from($this->sourcePath) as $file) {
			$this->addFile($file->getFilename());
		}
	}

}
