<?php

namespace h4kuna\Cms\Core\Caching;

use Nette\Caching as NCaching;

class CacheFactory
{

	/** @var NCaching\IStorage */
	private $storage;

	/** @var NCaching\Cache */
	private $cache;

	/** @var name */
	private $name = 'h4kuna';

	public function __construct(NCaching\IStorage $storage)
	{
		$this->storage = $storage;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function create($namespace)
	{
		return $this->getCache()->derive($namespace);
	}

	private function getCache()
	{
		if ($this->cache) {
			return $this->cache;
		}

		return $this->cache = new NCaching\Cache($this->storage, $this->name);
	}

}
