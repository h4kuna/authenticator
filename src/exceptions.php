<?php

namespace h4kuna\Cms;

abstract class Exception extends \Exception {}

// Development exceptions
class InvalidArgumentException extends Exception {}

class SourceFileDuplicityException extends Exception {}

// Authenticator
abstract class AuthenticatorException extends Exception {}

class IdentityNotFoundException extends AuthenticatorException {}

class InvalidPasswordException extends AuthenticatorException {}

class IdentityIsBlockedException extends AuthenticatorException {}

