<?php

namespace h4kuna\Cms\DI;

use Nette\DI as NDI,
	Nette\PhpGenerator;

class AuthenticatorExtension extends NDI\CompilerExtension
{

	public $defaults = [
		'identity' => [
			'removeColumns' => NULL,
		],
		'passwordLength' => 6,
		'usernameLength' => 3
	];

	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);
		if ($config['identity']['removeColumns'] === NULL) {
			$config['identity']['removeColumns'] = ['password', 'id'];
		}

		$builder = $this->getContainerBuilder();

		// cacheFactory
		$builder->addDefinition($this->prefix('cacheFactory'))
			->setClass('h4kuna\Cms\Core\Caching\CacheFactory');

		// textsFactory
		$cache = (new NDI\Statement('?->create(\'texts\')', [$this->prefix('@cacheFactory')]));
		$textsNeon = __DIR__ . '/../texts.neon';
		$textsFactory = $builder->addDefinition($this->prefix('textsFactory'));
		$textsFactory->setClass('h4kuna\Cms\Core\Translator\TextsFactory', [$cache])
			->setSetup([new NDI\Statement('?->addFile(?)', [$textsFactory, $textsNeon])])
			->setAutowired(FALSE);

		// texts
		$builder->addDefinition($this->prefix('texts'))
			->setClass('h4kuna\Cms\Core\Translator\Texts')
			->setFactory($this->prefix('@textsFactory::create'));

		// authenticator
		$builder->addDefinition($this->prefix('authenticator'))
			->setClass('h4kuna\Cms\Core\Security\Authenticator');

		// authenticatorAccessor
		$builder->addDefinition($this->prefix('authenticatorAccesor'))
			->setClass('h4kuna\Cms\Core\Security\Authenticator')
			->setImplement('h4kuna\Cms\Core\Security\AuthenticatorAccessor');

		// synchronizeIdentity
		$cache = (new NDI\Statement('?->create(\'synchronizeIdentity\')', [$this->prefix('@cacheFactory')]));
		$synchronizeIdentity = $builder->addDefinition($this->prefix('synchronizeIdentity'))
			->setClass('h4kuna\Cms\Core\Security\SynchronizeIdentity', [$cache]);

		// synchronizeIdentityChecker
		$builder->addDefinition($this->prefix('synchronizeIdentityChecker'))
			->setClass('h4kuna\Cms\Core\Security\SynchronizeIdentityChecker')
			->setAutowired(FALSE);

		// identityFactory
		$builder->addDefinition($this->prefix('identityFacotry'))
			->setClass('h4kuna\Cms\Core\Security\IdentityFactory', [$synchronizeIdentity, $config['identity']['removeColumns']]);

		// formFactory
		$builder->addDefinition($this->prefix('formFactory'))
			->setClass('h4kuna\Cms\Core\Form\FormFactory');

		// fields
		$builder->addDefinition($this->prefix('fields'))
			->setClass('h4kuna\Cms\Core\Form\Fields');

		// loginForm
		$builder->addDefinition($this->prefix('loginForm'))
			->setClass('h4kuna\Cms\Forms\LoginForm');

		// forgotPasswordForm
		$builder->addDefinition($this->prefix('forgotPasswordForm'))
			->setClass('h4kuna\Cms\Forms\ForgotPasswordForm');

		// registrationForm
		$registrationForm = $builder->addDefinition($this->prefix('registrationForm'))
			->setClass('h4kuna\Cms\Forms\RegistrationForm');
		$registrationForm->addSetup('?->setMinLength(?, ?)', [$registrationForm, $config['usernameLength'], $config['passwordLength']]);

		// restorePasswordForm
		$restorePasswordForm = $builder->addDefinition($this->prefix('restorePasswordForm'));
		$restorePasswordForm->setClass('h4kuna\Cms\Forms\RestorePasswordForm')
			->addSetup('?->setPasswordLength(?)', [$restorePasswordForm, $config['passwordLength']]);
	}

	public function afterCompile(PhpGenerator\ClassType $class)
	{
		$initialize = $class->getMethod('initialize');
		$initialize->addBody('$this->getService(\'' . $this->prefix('synchronizeIdentityChecker') . '\')->run();');
	}

}
